﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace IntroNunitTest
{
    [TestFixture]
    public class MyFirstClass
    {
        [Test]
        public void PositiveTest()
        {
            var x = 4;
            var y = 4;

            Assert.AreEqual(x, y);
        }

        //[Test]
        //public void NegativeTest()
        //{
        //    if(true)
        //    {
        //        Assert.Fail("Dupa dupa");
        //    }
        //}

        [Test, ExpectedException(typeof(NotSupportedException))]
        public void ExpectedException()
        {
            throw new NotSupportedException();
        }

        [Test,Ignore]
        public void NotImplementedException3()
        {
            throw new NotImplementedException();
        }

    }
}
